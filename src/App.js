import React, { Component } from "react";
import "./App.css";

// Used to camelcase custom fields
function camelize(str) {
  return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
    if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
    return index == 0 ? match.toLowerCase() : match.toUpperCase();
  });
}

// base Template
class Template {
  constructor(name, title, adCopy, campaignObjective) {
    this.name = name;
    this.title = title;
    this.adCopy = adCopy;
    this.campaignObjective = campaignObjective;
  }
}

// Define each template extending the base Template
class Template1 extends Template {
  constructor(
    name = "Single Image Ad",
    title = "Default Title",
    adCopy = "Default text",
    campaignObjective = "LeadGeneration"
  ) {
    super(name, title, adCopy, campaignObjective);
  }
}

class Template2 extends Template {
  constructor(
    name = "Multi Image Carousel Ad",
    /* Gonna go on a hunch here and assume this should say 'Title3' and not 'Titlet3' */
    title = ["Default Title1", "Default Title2", "Default Title3"],
    adCopy = ["Default Text1", "Default Text2", "Default Text3"],
    campaignObjective = "Conversions"
  ) {
    super(name, title, adCopy, campaignObjective);
  }
}

class Template3 extends Template {
  constructor(
    name = "Multi Image Slider Ad",
    /* Gonna go on a hunch here and assume this should say 'Title3' and not 'Titlet3' */
    title = ["Default Title1", "Default Title2", "Default Title3"],
    adCopy = ["Default Text1", "Default Text2", "Default Text3"],
    campaignObjective = "Impressions"
  ) {
    super(name, title, adCopy, campaignObjective);
  }
}

class App extends Component {
  constructor(props) {
    super(props);

    // Define the basic refs for the default template values
    this.nameRef = React.createRef();
    this.titleRef = React.createRef();
    this.adCopyRef = React.createRef();
    this.campaignObjectiveRef = React.createRef();
    this.chooserRef = React.createRef();

    // refs for custom parameters
    // TODO make dynamic so an unlimited amount can be added
    this.customElementValue1Ref = React.createRef();
    this.customElementName1Ref = React.createRef();
    this.customElementValue2Ref = React.createRef();
    this.customElementName2Ref = React.createRef();
    this.customElementValue3Ref = React.createRef();
    this.customElementName3Ref = React.createRef();
    this.customElementValue4Ref = React.createRef();
    this.customElementName4Ref = React.createRef();

    this.state = {
      // store all the campaigns
      campaigns: [],
      // templates used for the picker
      templates: [new Template1(), new Template2(), new Template3()]
    };
  }

  addCampaign() {
    let ind = this.chooserRef.current.value;
    // if the chosen item is an actual template
    if (this.state.templates[ind]) {
      // clone the current state's campaign
      let campaigns = [...this.state.campaigns];
      let newCampaign = null;
      // pick the campaign, default to 1
      switch (ind) {
        case "0":
          newCampaign = new Template1();
          break;
        case "1":
          newCampaign = new Template2();
          break;
        case "2":
          newCampaign = new Template3();
          break;
        default:
          newCampaign = new Template1();
      }
      // set the base properties of the campaign
      newCampaign.name = this.nameRef.current.value;
      newCampaign.title = this.titleRef.current.value;
      newCampaign.adCopy = this.adCopyRef.current.value;
      newCampaign.campaignObjective = this.campaignObjectiveRef.current.value;
      // iterate through the custom fields
      // TODO dynamically go through all refs that are not default fields
      // maybe create an array?
      if (this.customElementName1Ref.current.value != "") {
        newCampaign[
          camelize(this.customElementName1Ref.current.value)
        ] = this.customElementValue1Ref.current.value;
      }
      if (this.customElementName2Ref.current.value != "") {
        newCampaign[
          camelize(this.customElementName2Ref.current.value)
        ] = this.customElementValue2Ref.current.value;
      }
      if (this.customElementName3Ref.current.value != "") {
        newCampaign[
          camelize(this.customElementName3Ref.current.value)
        ] = this.customElementValue3Ref.current.value;
      }
      if (this.customElementName4Ref.current.value != "") {
        newCampaign[
          camelize(this.customElementName4Ref.current.value)
        ] = this.customElementValue4Ref.current.value;
      }
      // add the new campaign and set the state
      campaigns.push(newCampaign);
      this.setState({ campaigns });
    } else {
      // spawn an error if a valid template is not selected
      alert("Please select a template!");
    }
  }

  loadTemplate(event) {
    if (event > -1) {
      // load the default values of the selected template
      this.nameRef.current.value = this.state.templates[event].name;
      this.titleRef.current.value = this.state.templates[event].title;
      this.adCopyRef.current.value = this.state.templates[event].adCopy;
      this.campaignObjectiveRef.current.value = this.state.templates[
        event
      ].campaignObjective;
    }
  }

  publishCampaign = event => {
    let ind = event.currentTarget.value;
    if (ind > -1 && this.state.campaigns[ind]) {
      // if the campaign exists, publish it!
      // TODO add an actual publish function
      alert(`Published campaign ${this.state.campaigns[ind].name}`);
    }
  };

  render() {
    return (
      <div className="App">
        <div className="allCampaigns">
          {/* Render all the current campaigns, each with a publish button*/}
          {/* TODO shift the campaign display into a seperate component */}
          {Object.keys(this.state.campaigns).map(key => (
            <div className="campaign" key={key} index={key}>
              <div className="campaignName">
                {this.state.campaigns[key].name}
              </div>
              <button
                type="button"
                value={key}
                onClick={event => this.publishCampaign(event)}
              >
                Publish
              </button>
            </div>
          ))}
        </div>
        {/* Simple select element to allow template selection */}
        {/* TODO shift the template chooser and current campaign into a seperate component */}
        <select
          name="templateChooser"
          id="templateChooser"
          ref={this.chooserRef}
          onChange={event => this.loadTemplate(event.target.value)}
        >
          <option value="-1">Choose a template</option>
          {Object.keys(this.state.templates).map(key => (
            <option key={key} value={key}>
              {this.state.templates[key].name}
            </option>
          ))}
        </select>
        <div className="currentCampaign">
          <div className="campaignElement">
            <label htmlFor="name">Name:</label>
            <input
              type="text"
              name="name"
              ref={this.nameRef}
              placeholder="Name"
            />
          </div>
          {/* TODO add support for arrays/multiple values */}
          <div className="campaignElement">
            <label htmlFor="title">Title:</label>
            <input
              type="text"
              name="title"
              ref={this.titleRef}
              placeholder="Title"
            />
          </div>
          {/* TODO add support for arrays/multiple values */}
          <div className="campaignElement">
            <label htmlFor="adCopy">Ad Copy:</label>
            <input
              type="text"
              name="adCopy"
              ref={this.adCopyRef}
              placeholder="Ad Copy"
            />
          </div>
          <div className="campaignElement">
            <label htmlFor="campaignObjective">Campaign Objective:</label>
            <input
              type="text"
              name="campaignObjective"
              ref={this.campaignObjectiveRef}
              placeholder="Campaign Objective"
            />
          </div>
          {/* TODO rather than having 4 hard coded fields, ad a button to dynamically add input boxes (with refs?) which can be used for custom fields */}
          <div className="campaignElement">
            <input
              type="text"
              placeholder="Custom Element Name"
              ref={this.customElementName1Ref}
            />
            <input
              type="text"
              name="campaignObjective"
              ref={this.customElementValue1Ref}
              placeholder="Custom Element Value"
            />
          </div>
          <div className="campaignElement">
            <input
              type="text"
              placeholder="Custom Element Name"
              ref={this.customElementName2Ref}
            />
            <input
              type="text"
              name="campaignObjective"
              ref={this.customElementValue2Ref}
              placeholder="Custom Element Value"
            />
          </div>
          <div className="campaignElement">
            <input
              type="text"
              placeholder="Custom Element Name"
              ref={this.customElementName3Ref}
            />
            <input
              type="text"
              name="campaignObjective"
              ref={this.customElementValue3Ref}
              placeholder="Custom Element Value"
            />
          </div>
          <div className="campaignElement">
            <input
              type="text"
              placeholder="Custom Element Name"
              ref={this.customElementName4Ref}
            />
            <input
              type="text"
              name="campaignObjective"
              ref={this.customElementValue4Ref}
              placeholder="Custom Element Value"
            />
          </div>
        </div>

        <button
          type="button"
          onClick={() => {
            this.addCampaign();
          }}
        >
          Add Campaign
        </button>
      </div>
    );
  }
}

export default App;
